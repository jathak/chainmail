package com.jackthakar.chainmail;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import java.util.ArrayList;
import java.util.List;

public class ChainService extends Service {
    // Timing
    public static final int CONNECTION_WAIT = 1500;
    public static final int SCANNING_TIMEOUT = 6000;
    public static final int ATTEMPT_TIMEOUT = 2000;


    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private static final int ONGOING_NOTIFICATION_ID = 1;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothChatService mChatService;
    private MailCache mMailCache;


    public ChainService() {
    }

    @Override
    public int onStartCommand(Intent i, int flags, int startID){
        registerReceiver(mMessageReceiver,new IntentFilter(MainActivity.SEND_MESSAGE));
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new Notification.Builder(this)
                .setContentTitle("Chainmail")
                .setContentText("Open to Bluetooth Connections")
                .setSmallIcon(R.drawable.ic_launcher)
                .setPriority(Notification.PRIORITY_MIN)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(ONGOING_NOTIFICATION_ID, notification);
        setupBluetooth();
        return START_STICKY;
    }

    private void setupBluetooth() {
        mMailCache=new MailCache(this);
        mBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        mChatService=new BluetoothChatService(this,mHandler);
        mChatService.start();
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {case MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                    case BluetoothChatService.STATE_CONNECTED:
                        alreadyConnected=true;
                        String str=mMailCache.toString();
                        ChainService.this.sendMessage(str);
                        break;
                }
                break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    messageSent(writeMessage);
                    sentBlast=true;
                    if(receivedBlast){
                        blastDevices();
                    }
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    messageReceived(readMessage);
                    receivedBlast=true;
                    if(sentBlast){
                        blastDevices();
                    }
                    break;
            }
        }
    };

    private void messageSent(String sentMessage) {
        //TODO: React to sent message
    }
    private void messageReceived(String receivedMessage) {
        //TODO: React to received message
        if(receivedMessage.equals("nothing"))return;
        int oldSize = mMailCache.mails.size();
        mMailCache.initFromString(receivedMessage);
        //int newSize = mMailCache.mails.size();
        Intent i = new Intent(MainActivity.RECEIVE_MESSAGE);
        i.putExtra("oldSize",oldSize);
        sendBroadcast(i);
    }

    private void connectDevice(final String mac, final boolean secure){
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mac);
        //mChatService.connect(device, secure);
        currentDevice=device;
        connectWithDelay(device);
    }

    private void connectWithDelay(final BluetoothDevice device){
        mChatService.restart();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                mChatService.connect(device,false);
                printLog("Connecting to "+device.getName()+" ("+device.getAddress()+")");
                handleTimeout(device);
            }
        }, CONNECTION_WAIT);
    }
    private BluetoothDevice currentDevice;
    private boolean alreadyConnected = false;
    private void handleTimeout(final BluetoothDevice device){
        alreadyConnected=false;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if(device==currentDevice&&!alreadyConnected) {
                    mChatService.restart();
                    blastDevices();
                }
            }
        }, ATTEMPT_TIMEOUT);
    }

    private void printLog(String str){
        System.out.println(str);
        //TODO write logging
        Intent i = new Intent(MainActivity.RECEIVE_MESSAGE);
        i.putExtra("message",str);
        sendBroadcast(i);

    }
    private void sendMessage(String message) {
        if(message==null)message="no message";
        if(message.length()==0)message="nothing";
        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);
        }
    }
    private void scanForDevices(){
        deviceMacs=new ArrayList<String>();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);
        receiverRegistered=true;
        printLog("Starting discovery...");
        mBluetoothAdapter.startDiscovery();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                mBluetoothAdapter.cancelDiscovery();
            }
        }, SCANNING_TIMEOUT);
    }
    boolean receiverRegistered = false;
    private void blastDevices(){
        if(receiverRegistered){
            receiverRegistered=false;
            unregisterReceiver(mReceiver);
        }
        sentBlast=false;
        receivedBlast=false;
        if(deviceMacs==null)return;
        if(deviceMacs.size()>0){
            String mac = deviceMacs.remove(0);
            //printLog("Attempting connection to "+mac);
            //mChatService.restart();
            connectDevice(mac,false);
        }else{
            deviceMacs=null;
            //mChatService.restart();
            //printLog("Finished blast");

        }
    }

    private List<String> deviceMacs;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                //printLog("Found: "+device.getName()+" ("+device.getAddress()+")");
                if(!deviceMacs.contains(device.getAddress())&&device.getName()!=null)deviceMacs.add(device.getAddress());
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //printLog("Done scanning");
                blastDevices();
            }
        }
    };

    //private String currentMessage=null;

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            mMailCache.addMail(Mail.fromString(message));
            scanForDevices();

        }
    };

    @Override
    public void onDestroy(){
        unregisterReceiver(mMessageReceiver);
    }





    private boolean sentBlast=false,receivedBlast=false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
