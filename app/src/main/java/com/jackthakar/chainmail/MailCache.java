package com.jackthakar.chainmail;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jack on 4/12/14.
 */
public class MailCache {
    Context ctx;
    List<Mail> mails;
    String macAddress;
    public MailCache(Context ctx){
        this.ctx=ctx;
        mails=new ArrayList<Mail>();
        macAddress= BluetoothAdapter.getDefaultAdapter().getAddress();
        String str = ctx.getSharedPreferences("prefs",0).getString("cache", null);
        if(str!=null)initFromString(Utils.decryptText(str));
    }
    public void addMail(Mail m){
        for(Mail c:mails){
            if(m.isEqualTo(c))return;
        }
        m.addDevice(macAddress);
        mails.add(m);
        update();
    }
    private void update(){
        SharedPreferences.Editor e = ctx.getSharedPreferences("prefs",0).edit();
        if(mails.size()==0){
            e.putString("cache",null);
            e.commit();
            return;
        }
        String str = toString();
        String encrypted = Utils.encryptText(str);
        e.putString("cache",encrypted);
        e.commit();
    }
    public void addMail(String sender,String receiver,String message){
        addMail(new Mail(sender, receiver, message));
    }
    public String toString(){
        String str = "";
        boolean first=true;
        for(Mail m:mails){
            if(!first)str+="#";
            first=false;
            str+=m.toString();
        }
        return str;
    }
    public String getSubsetWithout(String macAddress){
        String str = "";
        boolean first=true;
        for(Mail m:mails){
            if(!m.devices.contains(macAddress)) {
                if (!first) str += "#";
                first = false;
                str += m.toString();
            }
        }
        return str;
    }
    public void initFromString(String str){
        if(str==null)return;
        for(String part:str.split("#")){
            addMail(Mail.fromString(part));
        }
    }
}
