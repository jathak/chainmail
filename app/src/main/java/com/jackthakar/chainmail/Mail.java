package com.jackthakar.chainmail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jack on 4/12/14.
 */
public class Mail {
    String sender,receiver,message;
    long timestamp;
    List<String> devices;

    public Mail(String sender,String receiver,String message){
        this.sender=sender;
        this.receiver=receiver;
        this.message = message;
        this.timestamp=System.currentTimeMillis();
        devices=new ArrayList<String>();
    }

    public void addDevice(String mac){
        if(devices.contains(mac))return;
        devices.add(mac);
    }

    public String toString(){
        String str = sender+";"+receiver+";"+Utils.encodeText(message)+";"+timestamp+";";
        boolean first = true;
        for(String d:devices){
            if(!first)str+=",";
            first=false;
            str+=d;
        }
        return str;
    }
    public boolean isEqualTo(Mail m){
        if(sender.equals(m.sender)&&receiver.equals(m.receiver)
                &&message.equals(m.message)&&timestamp==m.timestamp){
            for(String s:devices){
                m.addDevice(s);
            }
            return true;
        }
        return false;
    }

    public static Mail fromString(String str){
        System.out.println("Making mail: "+str);
        String[] parts = str.split(";");
        Mail m = new Mail(parts[0],parts[1],Utils.decodeText(parts[2]));
        m.timestamp=Long.parseLong(parts[3]);
        for(String sub:parts[4].split(",")){
            m.addDevice(sub);
        }
        return m;
    }
}
