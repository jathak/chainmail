package com.jackthakar.chainmail;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {
    public static final String SEND_MESSAGE = "com.jackthakar.chainmail.SEND_MESSAGE";
    public static final String RECEIVE_MESSAGE = "com.jackthakar.chainmail.RECEIVE_MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(MainActivity.this,ChainService.class));
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(MainActivity.this,ChainService.class));
            }
        });
        final BluetoothAdapter mBluetoothAdapter;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
            discoverableIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(discoverableIntent, 123);
        }else startService(new Intent(MainActivity.this,ChainService.class));
        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText et = (EditText)findViewById(R.id.message);
                EditText email = (EditText)findViewById(R.id.email);
                String message = et.getText().toString();
                String recipient = email.getText().toString();
                Mail m = new Mail(myEmail,recipient,message);
                m.addDevice(mBluetoothAdapter.getAddress());
                Intent i = new Intent(SEND_MESSAGE);
                i.putExtra("message",m.toString());
                sendBroadcast(i);
                TextView tv = (TextView)findViewById(R.id.log);
                tv.setText(tv.getText()+"\n"+myEmail+": "+message);
            }
        });
        AccountManager am = AccountManager.get(this);
        Account a = am.getAccounts()[0];
        String name = a.name;
        getActionBar().setSubtitle(name);
        myEmail=name;
    }
    private String myEmail;

    @Override
    public void onStart(){
        super.onStart();
        registerReceiver(mMessageReceiver,new IntentFilter(RECEIVE_MESSAGE));
    }

    @Override
    public void onStop(){
        super.onStop();
        unregisterReceiver(mMessageReceiver);
    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TextView tv = (TextView) findViewById(R.id.log);
            String message = intent.getStringExtra("message");
            if(message!=null){
                tv.setText(tv.getText() + "\n" + message);
            }
            int oldSize = intent.getIntExtra("oldSize",-1);
            if(oldSize>-1){
                MailCache mc = new MailCache(context);
                int diff = mc.mails.size()-oldSize;
                if(diff==0)tv.setText(tv.getText()+"\n"+"No new messages");
                else{
                    tv.setText(tv.getText()+"\n"+"Received "+diff+" new mails");
                   for(int i=oldSize;i<mc.mails.size();i++){
                       Mail m = mc.mails.get(i);
                       if(m.receiver.equals(myEmail)){
                           tv.setText(tv.getText()+"\n"+m.sender+": "+m.message);
                       }else{
                           tv.setText(tv.getText()+"\n"+m.sender+" No Auth: "+m.message);
                       }
                   }
                }
                tv.setText(tv.getText()+"\n"+"There are "+oldSize+" existing messages");
            }
        }
    };

    @Override
    public void onActivityResult(int request,int result,Intent i){
        if(request==123)startService(new Intent(MainActivity.this,ChainService.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
